# Trending on GitHub

Sample project showing most starred GitHub repos related to Android development.

To build app please make sure you have latest stable version of Android Studio and, most important, latest Kotlin support plugin.

#### Clarification on unclear task definition

Initial requirement to show "list of trending Github repositories of android" is not met. I had following options:

* Official GitHub API allows get everything related to Android but not trending
* Alternative APIs (such as https://github.com/huchenme/github-trending-api) allows to get trending repos but only in scope of programming language
* Parsing html output of https://github.com/trending/. I consider bad idea even for demo purpose

Best option to implement this requirement is to cache info for all ~800,000 repos related then filter alternative trending api output based on this cache.    

I believe that creativity should be offered by proposing solutions to the problem and discussing them. To invent something without making sure that it is required here is not the best way to get a result.

I deliberately preferred the easiest way to solve the problem. In the end, you gave me an evaluation task, I can do the same for you. Let's see what comes of it :)

#### Clarification on project dependencies

I preferred to use minimal set of dependencies for comfort of developing. I do not think using something just because it is _fancy_ is a good practice.

Dependency injection requires complex class structure to be useful. Reactive programming requires complex background tasks dependencies. Navigation controller requires complex requirement to screen set. We can discuss the measure of complexity. I prefer it following way: if there are more than 5 components _interconnected_, you should think about some non-trivial solution. There is nothing like this in my project.

#### Highlights on small features can be missed during review

* release keystore passwords are not stored in repo
* vector drawables
* AndroidX