package com.chaoticmanner.trendingrepo

import android.os.Bundle
import android.view.MenuItem

class ActivityMain : BaseActivity() {

    private val listFragment by lazy {
        val f = FragmentRepoList()
        f.onRepoInfo = { id -> showInfo(id) }
        f
    }

    private val infoFragment by lazy { FragmentRepoInfo() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFragment(R.id.Main_Content, listFragment)

        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount == 0) {
                supportActionBar?.setTitle(R.string.Common_AppName)
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
            } else {
                supportActionBar?.title = getFragment(R.id.Main_Content)?.getTitle() ?: ""
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showInfo(id: ApiRepoId) {
        setFragment(R.id.Main_Content, infoFragment, backstack = true)
        infoFragment.show(id)
    }
}
