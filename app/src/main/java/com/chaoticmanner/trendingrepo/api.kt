package com.chaoticmanner.trendingrepo

import androidx.annotation.Keep
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

data class ApiRepoOwner @Keep constructor(@JsonProperty("login") val name: String)

data class ApiRepoId(val owner: String, val name: String)

data class ApiRepoInfo @Keep constructor(@JsonProperty val name: String,
                                         @JsonProperty("description") val desc: String? = "",
                                         @JsonProperty private val owner: ApiRepoOwner,
                                         @JsonProperty("html_url") val url: String? = "",
                                         @JsonProperty("homepage") val site: String? = "",
                                         @JsonProperty val language: String? = "",
                                         @JsonProperty("stargazers_count") val stars: Int? = 0,
                                         @JsonProperty("subscribers_count") val watches: Int? = 0,
                                         @JsonProperty("forks_count") val forks: Int? = 0,
                                         @JsonProperty("open_issues_count") val issues: Int? = 0) {

    val id: ApiRepoId
        get() {
            return ApiRepoId(owner.name, name)
        }

    val fullName: String
        get() {
            return "${owner.name}/$name"
        }
}

data class ApiRepoSearch @Keep constructor(@JsonProperty("total_count") val total: Long,
                                           @JsonProperty("items") val data: List<ApiRepoInfo>)

interface ServiceGithub {

    @GET("search/repositories")
    fun searchRepos(@Query("q") phrase: String, @Query("sort") sort: String, @Query("order") order: String = "desc",
                    @Query("page") page: Int = 1, @Query("per_page") limit: Int = 20): Call<ApiRepoSearch>

    @GET("repos/{owner}/{name}")
    fun getRepo(@Path("owner") owner: String, @Path("name") name: String): Call<ApiRepoInfo>
}

object ServiceApi {

    private val http: OkHttpClient
    private val mapper = jacksonObjectMapper()
    val github: ServiceGithub

    init {
        val builder = OkHttpClient.Builder()
        val spec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).build()
        builder.connectionSpecs(Collections.singletonList(spec))

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            builder.addInterceptor(logging)
        }

        http = builder.build()

        mapper.disable(MapperFeature.AUTO_DETECT_CREATORS)
        mapper.disable(MapperFeature.AUTO_DETECT_FIELDS)
        mapper.disable(MapperFeature.AUTO_DETECT_GETTERS)
        mapper.disable(MapperFeature.AUTO_DETECT_SETTERS)
        mapper.disable(MapperFeature.AUTO_DETECT_IS_GETTERS)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)
        mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE)

        github = Retrofit.Builder().baseUrl("https://api.github.com/").addConverterFactory(JacksonConverterFactory.create(mapper)).client(http).build().create(ServiceGithub::class.java)
    }
}
