package com.chaoticmanner.trendingrepo

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.MvpDelegate

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private val mvp: MvpDelegate<out BaseActivity> by lazy { MvpDelegate(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvp.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        mvp.onAttach()
    }

    override fun onResume() {
        super.onResume()
        mvp.onAttach()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        mvp.onSaveInstanceState(outState)
        mvp.onDetach()
    }

    override fun onStop() {
        super.onStop()
        mvp.onDetach()
    }

    override fun onDestroy() {
        mvp.onDestroyView()
        if (isFinishing) mvp.onDestroy()
        super.onDestroy()
    }

    protected fun <F : Fragment> setFragment(@IdRes slot: Int, fragment: F, backstack: Boolean = false) {
        val trx = supportFragmentManager.beginTransaction()
        trx.replace(slot, fragment, fragment.javaClass.canonicalName)
        if (!backstack) trx.disallowAddToBackStack() else trx.addToBackStack(null)
        trx.commitAllowingStateLoss()
    }

    protected fun getFragment(slotId: Int = android.R.id.content): BaseFragment? {
        return supportFragmentManager.findFragmentById(slotId) as BaseFragment?
    }
}

open class BaseFragment : Fragment() {

    private var isSaved: Boolean = false
    private val mvp: MvpDelegate<out BaseFragment> by lazy { MvpDelegate(this) }

    init {
        retainInstance = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvp.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        isSaved = false
        mvp.onAttach()
    }

    override fun onResume() {
        super.onResume()
        isSaved = false
        mvp.onAttach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        isSaved = true
        mvp.onSaveInstanceState(outState)
        mvp.onDetach()
    }

    override fun onStop() {
        super.onStop()
        mvp.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mvp.onDetach()
        mvp.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (activity?.isFinishing == true) {
            mvp.onDestroy()
            return
        }

        if (isSaved) {
            isSaved = false
            return
        }

        var anyParentIsRemoving = false
        var parent: Fragment? = parentFragment
        while (!anyParentIsRemoving && parent != null) {
            anyParentIsRemoving = parent.isRemoving
            parent = parent.parentFragment
        }

        if (isRemoving || anyParentIsRemoving) {
            mvp.onDestroy()
        }
    }

    fun showMessage(@StringRes message: Int, callback: () -> Unit = {}) {
        val context = context ?: return
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.Common_OkButton) { _, _ -> callback.invoke() }
        builder.setOnCancelListener { callback.invoke() }
        builder.show()
    }

    open fun getTitle(): String {
        return ""
    }
}