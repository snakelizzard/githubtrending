package com.chaoticmanner.trendingrepo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.fragment_repolist.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync

class FragmentRepoList : BaseFragment(), RepoListView {

    @InjectPresenter
    lateinit var presenter: RepoListPresenter

    private val adapter = FlexibleAdapter<RepoItem>(null)
    private var hasMore = true
    var onRepoInfo: (ApiRepoId) -> Unit = { }

    init {
        adapter.addListener(FlexibleAdapter.OnItemClickListener { _, pos ->
            adapter.getItem(pos)?.let { onRepoInfo.invoke(it.value.id) }
            true
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repolist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sl = EndlessScrollListener()
        sl.onLoadMore = { fetchMore(it) }
        RepoList_Content.adapter = adapter
        RepoList_Content.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        RepoList_Content.addOnScrollListener(sl)
        RepoList_Refresh.isEnabled = false

        if (adapter.isEmpty) fetchMore(1)
    }

    override fun onDataAvailable(search: ApiRepoSearch) {
        RepoList_Refresh.isRefreshing = false
        hasMore = search.total > adapter.itemCount
        adapter.addItems(adapter.itemCount, search.data.map { RepoItem(it) })
    }

    override fun onError() {
        RepoList_Refresh.isRefreshing = false
        showMessage(R.string.Common_ErrorMessage)
    }

    private fun fetchMore(page: Int): Boolean {
        if (!hasMore) return false
        RepoList_Refresh.isRefreshing = true
        presenter.fetchMore(page)
        return true
    }
}

@InjectViewState
class RepoListPresenter : MvpPresenter<RepoListView>() {

    fun fetchMore(page: Int) {
        doAsync(exceptionHandler = { ex ->
            Log.e("Net", "Bad search", ex)
            GlobalScope.launch(Dispatchers.Main) { viewState.onError() }
        }) {
            val res = ServiceApi.github.searchRepos("android", sort = "stars", page = page).execute()

            GlobalScope.launch(Dispatchers.Main) {
                val search = res.body()
                if (res.isSuccessful && search != null) viewState.onDataAvailable(search)
                else viewState.onError()
            }
        }
    }
}

@StateStrategyType(OneExecutionStateStrategy::class)
interface RepoListView : MvpView {
    fun onDataAvailable(search: ApiRepoSearch)
    fun onError()
}

class RepoItemHolder @JvmOverloads constructor(view: View,
                                               adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>,
                                               sticky: Boolean = false) :
        FlexibleViewHolder(view, adapter, sticky) {

    fun set(value: ApiRepoInfo) {
        itemView.findViewById<TextView>(R.id.RepoItem_Name).text = value.fullName
        itemView.findViewById<TextView>(R.id.RepoItem_Desc).text = value.desc
        itemView.findViewById<TextView>(R.id.RepoItem_Stars).text = itemView.context.getString(R.string.RepoList_Stars, value.stars).parseAsHtml()
    }
}

class RepoItem(val value: ApiRepoInfo) : AbstractFlexibleItem<RepoItemHolder>() {

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>,
                                holder: RepoItemHolder, position: Int,
                                payloads: MutableList<Any>?) = holder.set(value)

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>) =
            RepoItemHolder(view, adapter)

    override fun equals(other: Any?) = value == other
    override fun getLayoutRes() = R.layout.view_repo
    override fun hashCode(): Int = value.hashCode()
}