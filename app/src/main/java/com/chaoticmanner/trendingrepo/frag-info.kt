package com.chaoticmanner.trendingrepo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import kotlinx.android.synthetic.main.fragment_repoinfo.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync

class FragmentRepoInfo : BaseFragment(), RepoInfoView {

    @InjectPresenter
    lateinit var presenter: RepoInfoPresenter

    private var id: ApiRepoId? = null
    private var info: ApiRepoInfo? = null

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repoinfo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        id?.let { show(it) }
        RepoInfo_Site.setOnClickListener { openUrl(info?.site ?: "") }
        RepoInfo_Refresh.isEnabled = false
    }

    override fun getTitle(): String {
        return getString(R.string.Main_RepoInfo)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.info, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.RepoInfo_Browse -> openUrl(info?.url ?: "")
            else -> return super.onOptionsItemSelected(item)
        }

        return true
    }

    override fun onDataAvailable(data: ApiRepoInfo) {
        RepoInfo_Refresh.isRefreshing = false
        info = data
        update()
    }

    override fun onError() {
        RepoInfo_Refresh.isRefreshing = false
        showMessage(R.string.Common_ErrorMessage)
    }

    fun show(id: ApiRepoId) {
        this.id = id
        info = null

        if (isVisible) {
            RepoInfo_Refresh.isRefreshing = true
            update()
            presenter.show(id)
        }
    }

    private fun update() {
        val site = info?.site ?: ""
        val lang = info?.language ?: ""

        RepoInfo_Name.text = info?.fullName ?: ""
        RepoInfo_Desc.text = info?.desc ?: ""
        RepoInfo_Site.text = getString(R.string.RepoInfo_Site, site).parseAsHtml()
        RepoInfo_Site.show(site.isNotEmpty())
        RepoInfo_Stars.text = getString(R.string.RepoInfo_Stars, info?.stars ?: 0).parseAsHtml()
        RepoInfo_Watch.text = getString(R.string.RepoInfo_Watch, info?.watches ?: 0).parseAsHtml()
        RepoInfo_Forks.text = getString(R.string.RepoInfo_Forks, info?.forks ?: 0).parseAsHtml()
        RepoInfo_Issue.text = getString(R.string.RepoInfo_Issue, info?.issues ?: 0).parseAsHtml()
        RepoInfo_Lang.text = lang
        RepoInfo_Lang.show(lang.isNotEmpty())
    }

    private fun openUrl(url: String) {
        if (url.isNotEmpty())
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            } catch (ex: Exception) {
                // nothing
            }
    }
}

@InjectViewState
class RepoInfoPresenter : MvpPresenter<RepoInfoView>() {

    fun show(id: ApiRepoId) {
        doAsync(exceptionHandler = { ex ->
            Log.e("Net", "Bad repo get", ex)
            GlobalScope.launch(Dispatchers.Main) { viewState.onError() }
        }) {
            val res = ServiceApi.github.getRepo(id.owner, id.name).execute()

            GlobalScope.launch(Dispatchers.Main) {
                val info = res.body()
                if (res.isSuccessful && info != null) viewState.onDataAvailable(info)
                else viewState.onError()
            }
        }
    }
}

@StateStrategyType(OneExecutionStateStrategy::class)
interface RepoInfoView : MvpView {
    fun onDataAvailable(data: ApiRepoInfo)
    fun onError()
}
