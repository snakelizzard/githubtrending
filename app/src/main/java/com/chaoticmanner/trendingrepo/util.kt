package com.chaoticmanner.trendingrepo

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class EndlessScrollListener : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5
    private var currentPage = 1
    private var previousTotalItemCount = 0
    private var loading = true
    private val startingPageIndex = 0
    var onLoadMore: (page: Int) -> Boolean = { false }

    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        if (view.layoutManager !is LinearLayoutManager) return
        val layoutManager = view.layoutManager as LinearLayoutManager
        val lastVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition()
        if (lastVisibleItemPosition == RecyclerView.NO_POSITION) return
        val totalItemCount = layoutManager.itemCount

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }

        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
            currentPage++
            loading = onLoadMore.invoke(currentPage)
        }
    }
}

fun CharSequence.parseAsHtml(flags: Int? = 0): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(toString(), flags ?: Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(toString())
    }
}

fun View.show(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}