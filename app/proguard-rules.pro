-dontnote **
-keepnames class kotlinx.** { *; }
-keep,includedescriptorclasses class com.chaoticmanner.trendingrepo.ApiRepo* { *; }

# OkHttp

-dontwarn okhttp3.**
-dontwarn okio.**

# Retrofit

-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

# Jackson

-dontwarn com.fasterxml.jackson.databind.ext.DOMSerializer
-dontwarn com.fasterxml.jackson.databind.ext.Java7SupportImpl

-keepclasseswithmembers,includedescriptorclasses class * {
    @com.fasterxml.jackson.annotation.JsonProperty <fields>;
}

-keepclasseswithmembers,includedescriptorclasses class * {
    @com.fasterxml.jackson.dataformat.xml.annotation.* <fields>;
}

-keep @com.fasterxml.jackson.annotation.JsonIgnoreProperties class * { *; }
-keep class com.fasterxml.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepclassmembers,includedescriptorclasses public final enum com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility {
    public static final com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility *;
}

# Moxy

-keep class **$$PresentersBinder
-keep class **$$State
-keep class **$$ParamsHolder
-keep class **$$ViewStateClassNameProvider
-keepnames class * extends com.arellomobile.mvp.*
